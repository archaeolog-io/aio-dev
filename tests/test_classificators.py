import unittest
import json

from tara.api import controller as api

UNITTEST_CLASSIFICATOR_0 = dict(
    name='Classificator 1',
    value='cls_1',
    type='MY_TYPE',
    sort=0
)

UNITTEST_CLASSIFICATOR_1 = dict(
    name='Classificator 2',
    value='cls_2',
    type='MY_TYPE',
    sort=0
)


class ClassificatorTestCase(unittest.TestCase):
    def setUp(self):
        app = api.create_app(TESTING=True)
        self.client = app.test_client()

    def test_create_classificator(self):
        # create category
        json_string = json.dumps(UNITTEST_CLASSIFICATOR_0)
        rv = self.client.post('/classificators/', data=json_string, content_type='application/json')
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # get category id
        classificator_id = json.loads(rv.data)['_id']
        self.assertEqual(len(classificator_id), 24)

        # update category
        url = '/classificators/{0}'.format(classificator_id)
        json_string = json.dumps(UNITTEST_CLASSIFICATOR_1)
        rv = self.client.patch(url, data=json_string, content_type='application/json')
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # delete category
        rv = self.client.delete(url)
        self.assertEqual(rv.status_code, 200, rv.data)


if __name__ == '__main__':
    unittest.main()
