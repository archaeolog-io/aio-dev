define(['backbone', 'models/classificator'], function (Backbone, ClassificatorModel) {
  "use strict";

  var ClassificatorCollection = Backbone.Collection.extend({
    url: '/api/classificators/',

    model: ClassificatorModel,

    parse: function (response) {
      return response.result;
    }
  });

  return ClassificatorCollection;
});
