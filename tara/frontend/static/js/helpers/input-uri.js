define(['backbone', 'backbone-forms'], function (Backbone) {
  "use strict";

  var InputUriEditor = Backbone.Form.editors.Text.extend({

	  tagName: 'textarea',
	
	  initialize: function(options) {
		  Backbone.Form.editors.Base.prototype.initialize.call(this, options);
	  }
	
	});
  return InputUriEditor;
});