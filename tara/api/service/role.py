from schema import Schema, And, Optional
from tara.core import mongo
from tara.api.service.exceptions import ResourceNotFound

NAME_SCHEMA = And(basestring, len)
DESC_SCHEMA = And(basestring)
PERMS_SCHEMA = [{
    'url': basestring,
    'methods': list,
    Optional('force_vars'):object,
    Optional('force_class'): dict
}]


class RoleService():
    def __init__(self):
        self.roles = mongo.db.roles

    def find(self, _id=None):
        if _id is None:
            return dict(result=self.roles.find())
        else:
            result = self.roles.find_one(_id)
            if result is None:
                raise ResourceNotFound()
            return result

    def insert(self, data):
        schema = Schema({
            'name': NAME_SCHEMA,
            Optional('description'): DESC_SCHEMA,
            'permissions': PERMS_SCHEMA
        })
        document = schema.validate(data)
        return self.roles.insert(document)

    def update(self, _id, data):
        schema = Schema({
            Optional('name'): NAME_SCHEMA,
            Optional('description'): DESC_SCHEMA,
            Optional('permissions'): PERMS_SCHEMA
        })
        document = schema.validate(data)
        self.roles.update(dict(_id=_id), {'$set': document})

    def remove(self, _id):
        self.roles.remove(_id)
