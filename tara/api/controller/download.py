from flask import Blueprint, request, current_app
from flask.views import MethodView
from time import time
from werkzeug.datastructures import Headers
from werkzeug.wsgi import wrap_file
from zlib import adler32

from tara.api.controller import register_api
from tara.api.service.download import DownloadService

from tara.restful_login import current_user

MAXIMUM_WIDTH_UNAUTHORIZED = 800
MAXIMUM_HEIGHT_UNAUTHORIZED = 600


class DownloadView(MethodView):
    def __init__(self):
        self.service = DownloadService()

    def get(self, _id):
        args = request.args

        # find file
        filedata = self.service.find(_id, args)

        # unauthorized users cannot download files other than jpeg's and they have w/h limit.
        if not current_user:
            if filedata.contentType != 'image/jpeg':
                return current_app.login_manager.unauthenticated()

            width = args.get('w', type=int)
            height = args.get('h', type=int)

            if width and height:
                if width > MAXIMUM_WIDTH_UNAUTHORIZED or height > MAXIMUM_HEIGHT_UNAUTHORIZED:
                    return current_app.login_manager.unauthenticated()
            else:
                return current_app.login_manager.unauthenticated()


        # file info
        upload_date = filedata.upload_date.strftime('%s')
        length = filedata.length

        headers = Headers()
        headers['Content-Length'] = length

        if args.get('dl', type=str) == 'true':
            headers.add('Content-Disposition', 'attachment', filename=filedata.filename)

        data = wrap_file(request.environ, filedata)
        rv = current_app.response_class(data, mimetype=filedata.content_type, headers=headers, direct_passthrough=True)

        # if we know the file modification date, we can store it as the
        # the time of the last modification.
        rv.last_modified = int(upload_date)

        cache_timeout = current_app.config['SEND_FILE_MAX_AGE_DEFAULT']

        rv.cache_control.public = True
        rv.cache_control.max_age = cache_timeout
        rv.expires = int(time() + cache_timeout)

        rv.set_etag('flask-%s-%s-%s' % (upload_date, length, adler32(str(filedata._id)) & 0xffffffff))

        return rv


bp = Blueprint('download', __name__, url_prefix='/download')
register_api(bp, DownloadView)
