define(['backgrid', 'translator'], function (Backgrid, T) {
  "use strict";

  var TemplateLayoutGrid = Backgrid.Grid.extend({
    initialize: function (options) {
      options.columns = [
        {
          name: 'label',
          label: T('layout-label'),
          editable: true,
          cell: 'string'
        },
        {
          name: 'sort',
          label: T('layout-sort'),
          editable: true,
          cell: 'integer'
        },
        {
          name: '_action',
          label: '',
          editable: false,
          cell: Backgrid.Cell.extend({
            render: function () {
              var edit_btn = '<a href="/templates/layouts/<%= _id %>" class="btn">Edit</a>'
              this.$el.html(_.template(edit_btn, this.model.attributes));
              return this;
            }
          })
        }
      ];
      this.constructor.__super__.initialize.apply(this, [options]);
    }
  });

  return TemplateLayoutGrid;
});
