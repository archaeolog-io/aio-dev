import unittest
import json

from tara.api import controller as api

UNITTEST_ROLE = dict(
    name='Test role',
    permissions=[dict(
        url='.*',
        methods=['GET', 'POST', 'PATCH', 'DELETE']
    )]
)

UNITTEST_USER_1 = dict(
    username='_unittestusername1_',
    password='_unittestpassword1_',
    first_name='John',
    last_name='Doe',
    is_active=True,
    email='user@wiseman.ee'
)

UNITTEST_USER_2 = dict(
    username='_unittestusername2_',
    password='_unittestpassword2_',
    first_name='Juhan',
    last_name='Tamm',
    is_active=True,
    email='user@wiseman.ee'
)


class UserTestCase(unittest.TestCase):
    def setUp(self):
        app = api.create_app(TESTING=True)
        self.client = app.test_client()

        rv = self.client.get('/users/')
        self.assertEqual(rv.status_code, 200)

        dumped = json.loads(rv.data)
        to_remove = filter(lambda u: u['username'] in [UNITTEST_USER_1['username'], UNITTEST_USER_2['username']], dumped['result'])

        for item in to_remove:
            app.logger.warn('Cleaning up previous tests')
            self.test_delete_user(item['_id'])
            self.client.delete('/roles/'+item['role'])

        rv = self.client.post('/roles/', data=json.dumps(UNITTEST_ROLE), content_type='application/json')
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')
        role_id = json.loads(rv.data)['_id']

        UNITTEST_USER_1['role'] = role_id
        UNITTEST_USER_2['role'] = role_id

    def test_delete_user(self, _id=None):
        if _id:
            rv = self.client.delete('/users/' + _id)
            self.assertEqual(rv.status_code, 200)

    def test_create_user(self, cleanup=True):
        json_string = json.dumps(UNITTEST_USER_1)

        # create user
        rv = self.client.post('/users/', data=json_string, content_type='application/json')
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # get user id
        _id = json.loads(rv.data)['_id']
        self.assertEqual(len(_id), 24)

        if cleanup:
            self.test_delete_user(_id)

        return _id

    def test_update_user(self):
        # create user
        _id = self.test_create_user(cleanup=False)
        json_string = json.dumps(UNITTEST_USER_2)

        # update user
        rv = self.client.patch('/users/'+_id, data=json_string, content_type='application/json')
        self.assertEqual(rv.status_code, 200, str(rv.data))
        self.assertEqual(rv.content_type, 'application/json')

        # delete user
        rv = self.client.delete('/users/' + _id)
        self.assertEqual(rv.status_code, 200)

        # delete non-existing user
        rv = self.client.delete('/users/' + _id)
        self.assertEqual(rv.status_code, 404)

        # try retrive deleted user
        rv = self.client.get('/users/'+_id)
        self.assertEqual(rv.status_code, 404)