define(['backbone', 'views/user/form', 'models/user', 'collections/role', 'text!/static/templates/user/add.html'], function (Backbone, UserForm, UserModel, RolesCollection, userAddTmpl) {
  "use strict";

  var UserAddView = Backbone.View.extend({
    template: _.template(userAddTmpl),

    events: {
      'click #save-item': function (e) {
        e.preventDefault();
        this.form.commit();
      }
    },

    initialize: function () {
      this.model = new UserModel();
      this.listenTo(this.model, 'sync', this.redirect);

      this.roles = new RolesCollection();
    },

    render: function () {
      var self = this;
      this.roles.fetch({success: function (roles) {
        self.form = new UserForm({template: self.template, model: self.model, roles: roles});
        self.$el.html(self.form.render().el);
      }});
      return this;
    },

    redirect: function () {
      Backbone.history.navigate('users', {trigger: true});
    },

    onClose: function () {
      this.form.remove();
    }
  });

  return UserAddView;
});
