define(['collections/template_layout', 'views/template/layout/grid', 'text!/static/templates/template/layout/list.html'],
function (TemplateLayoutCollection, TemplateLayoutGrid, templateLayoutListTmpl) {

  "use strict";

  var TemplateLayoutListView = Backbone.View.extend({
    template: _.template(templateLayoutListTmpl),

    initialize: function () {
      this.collection = new TemplateLayoutCollection();
      this.grid = new TemplateLayoutGrid({collection: this.collection});
    },

    render: function () {
      this.$el.append(this.template({permissions: Backbone.View.permissions()}));
      this.$el.find('#grid-content').append(this.grid.render().$el);
      this.collection.fetch({reset: true});
      return this;
    },

    onClose: function () {
      this.grid.remove();
    }
  });

  return TemplateLayoutListView;
});
