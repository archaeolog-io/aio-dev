import bson
import re
from gridfs import GridFS
from datetime import datetime
from mimetypes import guess_type
from werkzeug.datastructures import FileStorage
from schema import Schema, Optional, And, Or, Use
from tara.core import mongo
from tara.restful_login import current_user
from tara.api.service.exceptions import ResourceNotFound, SearchQueryError

RESERVED_ARGS = ['page', 'per_page', 'sort_by', 'order', 'total_pages', 'total_entries']

DISPLAY_FIELDS = {'chunkSize': False}

INSERT_SCHEMA = Schema(FileStorage)

UPDATE_SCHEMA = Schema({
    Optional('title'): And(basestring, len),
    Optional('author'): And(basestring),
    Optional('description'): And(basestring),
    Optional('notes'): And(basestring),
    Optional('categories'): [And(basestring, len)],
    Optional('references'): [{
        'resource_id': Or(None, And(basestring, lambda s: bson.ObjectId.is_valid(s), Use(lambda s: bson.ObjectId(s)))),
        'template_id': Or(None, And(basestring, len, lambda s: bson.ObjectId.is_valid(s), Use(lambda s: bson.ObjectId(s)))),
        Optional('description'): basestring
    }]
})


class FileService():
    def __init__(self, base='fs'):
        self.storage = GridFS(mongo.db, base)
        self.files = mongo.db[base].files

    def find(self, _id, args=None):
        if _id is None:
            query = {}

            try:
                if args:
                    for key, values in args.iterlists():
                        if key not in RESERVED_ARGS:
                            if len(values) == 1 and len(values[0]):
                                if key in ['references.template_id', 'references.resource_id']:
                                    query[key] = bson.ObjectId(values[0])
                                elif key == 'categories':
                                    query[key] = {'$in': [values[0]]}
                                else:
                                    query[key] = {'$regex': re.compile(values[0], re.IGNORECASE)}
                            elif len(values) > 1:
                                query['$or'] = map(lambda x: {key: {'$regex': re.compile(x, re.IGNORECASE)}}, values)

            except re.error:
                raise SearchQueryError

            # cursor
            cursor = self.files.find(query, fields=DISPLAY_FIELDS)

            # sorting
            if args:
                if args.get('sort_by'):
                    sort_by = args.get('sort_by', type=str)
                    order = args.get('order', type=int)
                    cursor.sort(sort_by, order)

                # skipping
                if args.get('page'):
                    page = args.get('page', type=int)
                    per_page = args.get('per_page', type=int)
                    cursor.skip((page-1) * per_page).limit(per_page)

            # return result
            return dict(result=cursor, total_entries=cursor.count())
        else:
            result = self.files.find_one(_id, fields=DISPLAY_FIELDS)
            if result is None:
                raise ResourceNotFound()
            return result

    def insert(self, data):
        INSERT_SCHEMA.validate(data)
        content_type = guess_type(data.filename)[0]

        document = dict(title=data.filename, filename=data.filename, content_type=content_type)

        if current_user:
            document['_creator_id'] = current_user['_id']
            document['_creator_name'] = current_user['full_name']

        return self.storage.put(data, **document)

    def update(self, _id, data):
        document = UPDATE_SCHEMA.validate(data)
        document['_updated'] = datetime.utcnow()

        if current_user:
            document['_editor_id'] = current_user['_id']
            document['_editor_name'] = current_user['full_name']

        self.files.update(dict(_id=_id), {'$set': document})

    def remove(self, _id):
        self.storage.delete(_id)

        # clear cache
        for cached in mongo.db['cache'].files.find({'file_id': _id}):
            mongo.db['cache'].files.remove({'_id': cached['_id']})
            mongo.db['cache'].chunks.remove({'files_id': cached['_id']})
