define(['backbone', 'collections/file', 'collections/classificator', 'models/file', 'text!/static/templates/file/add_existing.html', 'text!/static/templates/file/add_existing_row.html', 'translator'], function (Backbone, FileCollection, ClassificatorCollection, FileModel, fileAddExistingTmpl, fileAddExistingRowTmpl, T) {
  "use strict";

  var FileListItem = Backbone.View.extend({
    template: _.template(fileAddExistingRowTmpl),

    tagName: "li",

    className: "span3",

    events: {
        "click a": function (e) {
          e.preventDefault();

          var template_id = this.options.params.template_id;
          var resource_id = this.options.params.resource_id;

          var references = this.model.get('references') || [];

          var idx_to_remove = -1;

          // return when reference exists
          for (var i = 0; i < references.length; i++) {
            var ref = references[i];
            if (ref.template_id == template_id && ref.resource_id == resource_id) {
              idx_to_remove = i;
              break;
            }
          }

          if (idx_to_remove != -1) {
            references.splice(idx_to_remove, 1);
            this.$el.find('.is_linked').hide();
          } else {
            references.push({template_id: template_id, resource_id: resource_id});
            this.$el.find('.is_linked').show();
          }

          var fileModel = new FileModel({_id: this.model.id});
          fileModel.set({'references': references});
        }
    },

    render: function () {
      var html = this.template(this.model.attributes);
      this.$el.append(html);

      var references = this.model.get('references') || [];

      // find if already referenced
      for (var i = 0; i < references.length; i++) {
        var ref = references[i];
        if (ref.template_id == this.options.params.template_id && ref.resource_id == this.options.params.resource_id) {
          this.$el.find('.is_linked').show();
          break;
        }
      }

      return this;
    }
  });

  var FileListView = Backbone.View.extend({
    tagName: "ul",

    className: "thumbnails",

    render: function () {
      var self = this;
      this.collection.each(function (model) {

        var fileListItem = new FileListItem({model: model, params: self.options.params});
        self.$el.append(fileListItem.render().$el);
      });
      return this;
    }
  });

  var FileAddExistingView = Backbone.View.extend({
    search: {
      title: T('file-title'),
      author: T('file-author'),
      description: T('file-description'),
      notes: T('file-notes'),
     'references.template_id': T('file-reference-template-id'),
     'references.resource_id': T('file-reference-resource-id')
    },

    events: {
      'click #go_back': function (e) {
        window.history.back();
      },
      'change #category-filter': function (e) {
        var value = e.target.value;
        if (value) {
          this.collection.queryParams['categories'] = value;
        } else {
          delete this.collection.queryParams['categories'];
        }
        this.collection.fetch();
      },

      'submit form': function (e) {
        var self = this;
        e.preventDefault();

        var key = e.target[1].value;
        var value = e.target[2].value;
        var params = self.collection.queryParams;

        if (key && value) {
          if (params[key]) {
            if (!_.isArray(params[key])) {
              params[key] = [params[key]];
            }
            if (_.indexOf(params[key], value) == -1) {
              params[key].push(value);
            }
          } else {
            params[key] = value;
          }
          this.collection.fetch();
        }
      },

      /* Remove filter */
      'click span[class="label"]': function (e) {
        var key = $(e.target).attr('data-key');
        var val  = $(e.target).attr('data-val');
        var params = this.collection.queryParams;

        if (_.isArray(params[key])) {
          var pos = _.indexOf(params[key], val);
          if (pos != -1) {
            params[key].splice(pos, 1);
          }
        } else {
          delete params[key];
        }

        this.collection.fetch();
      }
    },

    initialize: function (options) {
      this.templateCollection  = options.templateCollection;
      this.params = options.params;

      this.collection = new FileCollection();
      this.reserved_qp = _.clone(this.collection.queryParams);

      this.paginator = new Backgrid.Extension.Paginator({collection: this.collection});

      // event used for hidding or displaying paginator, depending on page count
      this.listenTo(this.collection, 'sync', this.render_files);

      this.categories = new ClassificatorCollection();
      this.listenTo(this.categories, 'sync', this.render_category_filter);
    },

    render: function () {
      var html = _.template(fileAddExistingTmpl, {search: this.search});
      this.$el.append(html);

      this.collection.fetch();
      this.categories.fetch({data: {type: 'FILE_CATEGORIES'}});

      return this;
    },

    render_files: function () {
      this.$el.find('#files-container').empty();

      // render file list
      var fileListView = new FileListView({collection: this.collection, params: this.params});
      this.$el.find('#files-container').append(fileListView.render().$el);

      this.$el.find('#filter-list').empty();

      var filter_generator = function (key, value, label) {
        var str = _.str.sprintf('<span><span class="label" data-key="%1$s" data-val="%2$s">%3$s = %2$s</span>&nbsp;&nbsp;</span>', key, value, label);
        return $(str);
      };

      var self = this;

      _.each(this.collection.queryParams, function (val, key) {
        if(!_.has(self.reserved_qp, key)) {
          if (key != 'categories') {
            if (_.isArray(val)) {
              _.each(val, function (xval) {
                var html = filter_generator(key, xval, self.search[key]);
                self.$el.find('#filter-list').append(html);
              });
            }
            var html = filter_generator(key, val, self.search[key]);
            self.$el.find('#filter-list').append(html);
          }
        }
      });

      // render paginator
      this.$el.find('#paginator').append(this.paginator.render().$el);
    },

    render_category_filter: function (categories) {
      var options = this.$el.find('#category-filter');
      categories.forEach(function (model) {
        options.append($('<option />').val(model.get('value')).text(model.get('name')));
      });

      // set selectbox value
      var val = this.collection.queryParams['categories'];
      if (val) {
        this.$el.find('#category-filter').val(val);
      }
    }
  });

  return FileAddExistingView;
});
