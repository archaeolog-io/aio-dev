define(['collections/category', 'views/category/grid', 'text!/static/templates/category/list.html'], function (CategoryCollection, CategoryGrid, categoryListTmpl) {
  "use strict";

  var CategoryListView = Backbone.View.extend({
    template: _.template(categoryListTmpl),

    initialize: function () {
      this.collection = new CategoryCollection();
      this.grid = new CategoryGrid({collection: this.collection});
    },

    render: function () {
      this.$el.append(this.template());
      this.$el.find('#grid-content').append(this.grid.render().$el);
      this.collection.fetch({reset: true});
      return this;
    },

    onClose: function () {
      this.grid.remove();
    }
  });

  return CategoryListView;
});
