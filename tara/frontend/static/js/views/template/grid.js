define(['backgrid', 'translator'], function (Backgrid, T) {
  "use strict";

  var TemplateGrid = Backgrid.Grid.extend({
    initialize: function (options) {
      options.columns = [
        {
          name: 'label',
          label: T('template-label'),
          editable: true,
          cell: 'string'
        },
        {
          name: 'parent',
          label: T('template-parent'),
          editable: false,
          cell: 'string'
        },
        {
          name: 'layout',
          label: T('template-layout'),
          editable: false,
          cell: 'string'
        },
        {
          name: 'sort',
          label: T('template-sort'),
          editable: true,
          cell: 'integer'
        },
        {
          name: '_action',
          label: '',
          editable: false,
          cell: Backgrid.Cell.extend({
            render: function () {
              var edit_btn = '<a href="/templates/<%= _id %>" class="btn">' + T('button-edit') + '</a>'
              this.$el.html(_.template(edit_btn, this.model.attributes));
              return this;
            }
          })
        }
      ];
      this.constructor.__super__.initialize.apply(this, [options]);
    }
  });

  return TemplateGrid;
});
