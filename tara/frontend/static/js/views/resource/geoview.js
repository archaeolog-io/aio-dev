define(['collections/resource', 'models/autocomplete', 'views/resource/grid', 'ucsv', 'text!/static/templates/resource/geoview.html', 'proj4', 'translator', 'openlayers','jquery-ui'],
function (ResourceCollection, AutocompleteModel, ResourceGrid, CSV, resourceListTmpl, proj4, T, OL, JU) {
  "use strict";

  var ResourceGeoView = Backbone.View.extend({
    template: _.template(resourceListTmpl),

    events: {
      'click #filter-template': function (e) {
        if ($(e.target).is(':checked') == true) {
          this.resourceCollection.templateId = this.templateCollection.get(this.templateId).get('parent');
        } else {
          this.resourceCollection.templateId = this.templateId;
        }
        this.fetchLayer();
      },

      'change #page-size': function (e) {
        if(e.target.value) {
          var val = parseInt(e.target.value);
          this.resourceCollection.setPageSize(val);
        }
      },

      /* Update autocomplete searching */
      'change #field-filter': function (e) {
        var self = this;
        var selected = $(e.target).find('option:selected');

        if (e.target.value && selected.data('type') != 'number') {
          var input = this.$el.find('input');
          input.val('');
          input.typeahead({
            source: function (query, process) {
              var autocomplete = new AutocompleteModel({}, {templateId: self.templateId, field: e.target.value});
              autocomplete.fetch({success: function () {
                process(autocomplete.toJSON().result);
              }});
            },
            updater: function (item) {
              return '^' + item + '$';
            }
          });
        }
      },

      /* Filter search form submit */
      'submit form': function (e) {
        e.preventDefault();

        var key = e.target[0].value;
        var val = e.target[1].value;

        if (key && val) {
          var qp = this.resourceCollection.queryParams;
          if (qp[key]) {
            if (!_.isArray(qp[key])) {
              qp[key] = [qp[key]];
            }
            if (_.indexOf(this.resourceCollection.queryParams[key], val) == -1) {
              qp[key].push(val);
            }
          } else {
            qp[key] = val;
          }
        } else {
          return;
        }

        // reset input value
        $(e.target[1]).val('');

        this.fetchLayer();

        // go to first page
        this.resourceCollection.state.currentPage = 1;
      },

      /* Remove filter */
      'click span[class="label"]': function (e) {
        var result = e.target.textContent.split('=');
        var key = result[0];
        var val = result[1];

        // Removes item from array (if it is array)
        var qp = this.resourceCollection.queryParams;

        if (qp[key]) {
          if (_.isArray(qp[key])) {
            var pos = _.indexOf(qp[key], val);
            if (pos != -1) {
              qp[key].splice(pos, 1);
            }
          } else {
            delete qp[key];
          }
        }

        e.target.remove();
        this.fetchLayer();

        // go to first page
        this.resourceCollection.state.currentPage = 1;
      }
    },

    initialize: function (options) {
      // Set up proj4 object globally fo ol to access
    	window.proj4=proj4;
    	// L-EST 97 Estonian data
    	window.proj4.defs("EPSG:3301","+proj=lcc +lat_1=59.33333333333334 +lat_2=58 +lat_0=57.51755393055556 +lon_0=24 +x_0=500000 +y_0=6375000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
    	// Projection for Yandex
    	window.proj4.defs('EPSG:3395', '+proj=merc +datum=WGS84 +ellps=WGS84 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +no_defs')
    	
      this.templateId = options.templateId;
      this.templateCollection = options.templateCollection;

      this.resourceCollection = new ResourceCollection([], {templateId: options.templateId});
      this.reserved_qp = _.clone(this.resourceCollection.queryParams);
      
      //this.layerCollection = new ResourceCollection([], {templateId: '5491833cdf27490b64761eb2'});
      

      // set sorting
      if (options.query && _.has(options.query, 'sort_by') && _.has(options.query, 'order')) {
        this._sort_by = options.query['sort_by'];
        this._order = options.query['order'];
        delete options.query['sort_by'];
        delete options.query['order'];
      }

      // set page
      if (options.query && _.has(options.query, 'page')) {
        this.resourceCollection.state['currentPage'] = parseInt(options.query['page']);
        delete options.query['page'];
      }

      // set page size
      if (options.query && _.has(options.query, 'per_page')) {
        this.resourceCollection.setPageSize(parseInt(options.query['per_page']));
        delete options.query['per_page'];
      }
      else {
    	  this.resourceCollection.setPageSize(parseInt(250));
      }
      // query show_all
      if (options.query && _.has(options.query, 'show_all')) {
        this.show_all = true;
        delete options.query['show_all'];
      }

      // set query params
      var self = this;
      _.each(options.query, function (val, key) {
        self.resourceCollection.queryParams[key] = val;
      });

      // event used for hidding or displaying paginator, depending on page count
      this.listenTo(this.resourceCollection, 'sync', this.after_sync);
    },

    display_badge: function (key, val) {
      var el = $('<span><span class="label">' + key + '=' + val +'</span>&nbsp;&nbsp;</span>');
      this.$el.find('#filter-list').append(el);
    },

    after_sync: function (collection) {
      var self = this;
      this.$el.find('#filter-list').empty();

      var url = Backbone.history.fragment;

      // clear url query params
      if (_.contains(url, '?')) {
        url = url.split('?')[0];
      }

      // starting of query params
      url += '?';

      _.each(this.resourceCollection.queryParams, function (val, key) {
        if(!_.has(self.reserved_qp, key)) {
          if (_.isArray(val)) {
            _.each(val, function (val) {
              self.display_badge(key, val);
              url += key + '=' + val + '&';
            });
          } else {
            self.display_badge(key, val)
            url += key + '=' + val + '&';
          }
        }
      });

      // add sort by query param
      if (this.resourceCollection.state['sortKey']) {
        url += 'sort_by' + '=' + this.resourceCollection.state['sortKey'] + '&';

        var order = this.resourceCollection.state['order'] == -1 ? 'ascending' : 'descending';
        url += 'order' + '=' + order + '&';
      }

      // add current page
      if (this.resourceCollection.state['currentPage'] && this.resourceCollection.state['currentPage'] != 1) {
        url += 'page' + '=' + this.resourceCollection.state['currentPage'] + '&';
      }

      // add page size
      if (this.resourceCollection.state['currentPage'] && this.resourceCollection.state['currentPage'] != 1) {
        url += 'per_page' + '=' + this.resourceCollection.state['pageSize'] + '&';
      }

      if (this.resourceCollection.templateId != this.templateId) {
        url += 'show_all' + '=true&';
        this.$el.find('#filter-template').prop('checked', true);
      }

      // remove last &
      url = url.substring(0, url.length - 1);

      // set new url if currnet url ne composed url
      if (decodeURIComponent(Backbone.history.fragment) != url) {
        Backbone.history.navigate(url);
      }

      // show user total count of items in collection
      this.$el.find('#total-count').show();
      this.$el.find('#total-count-nr').html(collection.state.totalRecords);
    },
    getIcon: function(feature) {
    	
    	var times={
			'SA':'1',
			'MSA':'1',
			'NSA':'1',
			'BA':'2',
			'EBA':'2',
			'LBA':'2',
			'IA':'4',
			'EIA':'3',
			'EIApr':'3',
			'EIArm':'3',
			'MIA':'4',
			'MIAmig':'4',
			'MIApvi':'4',
			'LIA':'6',
			'LIAvi':'5',
			'LIAfi':'7',
			'MA':'8',
			'EMT':'9'
		};
    	var mt=feature.monument_type.split(';')[0].toLowerCase();
    	var ot=feature.object_type.split(';')[0].toLowerCase();
    	ot = ot.replace('kapulauks', '');
    	var p2=[];
    	
    	var noperiod=['christian cemetery','flat cremation cemetery','sacred tree','cross stone','cult stone',
    	              'zhalnik cemetery','cup-marked stone','stone cross','cult mountain','cult hill','sacred spring',
    	              'sacred tree / grove','chapel','sacral place','barrow-zhalnik cemetery','']
    	
    	var ps=feature.period.split(';');
    	for	(var index = 0; index < ps.length; index++) {
    		
    	    if (index<3) {
    	    	ps[index]=ps[index].replace('?','').trim();
    	    	if (times[ps[index]]) {
    	    		p2.push(times[ps[index]]);
    	    	}
    	    	
    	    } 
    	}
    	if (mt=='dwelling site') {
    		var iconname=mt+p2.join('.');
    	}
    	else {
    	
    		ot=ot.replace('?','').trim();
    		var iconname=ot;
    		if (noperiod.indexOf(ot)==-1) {
    			iconname=iconname+p2.join('.');
    		}
    	}
    	
    	var empty = ' ';
    	var re = new RegExp(empty, 'g');

    	iconname = iconname.replace(re, '_');
    	
    	if (iconname.length==0) iconname='stray_find';
    	iconname = iconname.replace('/','_');
    	return(iconname+'.png');
    },
    setVectorStyle: function() {
		  var stroke = new ol.style.Stroke({
  		    color: 'blue'
  		  });
  		  var textStroke = new ol.style.Stroke({
  		    color: '#fff',
  		    width: 3
  		  });
  		  var textFill = new ol.style.Fill({
  		    color: '#000'
  		  });
  		  
  		  var minResolution=300;
  		  var min2Resolution=75;
  		  return function(feature, resolution) {
  			  var t='';
  			  var fontsize='12px';
  			  if (resolution>min2Resolution) var fontsize='10px';
  			  if (resolution<minResolution) t=feature.get('site_id');
  			  if (resolution<min2Resolution) t=feature.get('site_id')+' '+feature.get('name');
  			  
  		    return [new ol.style.Style({
  		    	image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
  		    	    anchor: [0.5, 0.5],
  		    	    anchorXUnits: 'fraction',
  		    	    anchorYUnits: 'fraction',
  		    	    opacity: 0.75,
  		    	    src: feature.get('icon')
  		    	  })),
      		      stroke: stroke,
      		      text: new ol.style.Text({
      		        font: fontsize+' Calibri,sans-serif',
      		        text: t,
      		        fill: textFill,
      		        stroke: textStroke,
      		        offsetY: -12,
      		        offsetX: 12
      		      })
  		    })];
  		  };
  		},
    fetchLayer: function() {
    	self.features=new Array();
    	var that=this;
    	this.resourceCollection.fetch({
    		success : function(collection, data) {
    		    // code here
    			var i=0;
    		  data.result.forEach(function(item) {
    			  if (item.latitude && item.longitude) {
    				  var coordinates=ol.proj.transform([parseFloat(item.longitude),parseFloat(item.latitude)], 'EPSG:4326', self.vectorProjection);
    				  self.features[i]=new ol.Feature({geometry:new ol.geom.Point(coordinates),
    				  url:"/resources/"+item._template_id+"/"+item._id,
    				  al1_mod:item.al1_mod,
    				  al2_mod:item.al2_mod,
    				  al3_mod:item.al3_mod,
    				  al4_mod:item.al4_mod,
    				  monument_type:item.monument_type,
    				  object_type:item.object_type,
    				  period:item.period,
    				  site_id:item.site_id,
    				  name:item.name,
    				  toponym:item.toponym,
    				  icon:'/static/themes/default/icons/'+that.getIcon(item)
    				  });
    				  // Lisa ikoonid ja popup
    				  i=i+1;
    			  }
    			  
    		  });
    		  
    		  self.source.clear();
    		  self.source.addFeatures(self.features);
    		  
    		},

    		  error : function(collection, response) {
    		    // code here
    			  console.log(response)
    		  }
    		});
    	
    	
    	
    },
    switchLayer:function() {
    	var checkedLayer = $('#layerswitcher input[name=layer]:checked').val();
    	var i;var ii;
    	for (i = 0, ii = self.layers.length; i < ii; ++i) {
    		if (i==checkedLayer) {
    			self.activeLayer=i;
    			if (self.tileProjection!=self.opts['layers'][i]['projection']) {
    				this.destroyMap();
        			this.fetchMap();
    			}
    		} 
    		
    		self.layers[i].setVisible(i==checkedLayer);
    	}
    },
    /*http://xbb.uz/openlayers/i-Yandex.Maps*/
    yandex_getTileURL: function(tileCoord, pixelRatio, projection) {
    	  var x = tileCoord[1];
    	  var y = tileCoord[2];
    	  var zoom = tileCoord[0];
    	  var layer=self.opts["layers"][self.activeLayer]['layer'];
    	  var server="vec0";
    	  if (layer=="sat") server="sat0";
            return "http://"+server+((x+y)%5)+".maps.yandex.net/tiles?l="+layer+"&v=2.16.0&x=" + 
                x + "&y=" + y + "&z=" + zoom + "";
    },
    getConfig:function() {
    	//find templateID for map_layers collection
    	var that=this;
    	 this.templateCollection.fetch({reset: true, success: function (dd) {
    		 var d=dd['models'];
    		 //var t_id='5491833cdf27490b64761eb2';
    		 var t_id='map_layers_not_in_use';
    		 for (var index = 0; index < d.length; ++index) {
 			    
    			if (d[index]['attributes']['collection']=='map_layers') {
    				t_id=d[index]['id'];
    			}
 			 }
    		 that.layerCollection = new ResourceCollection([], {templateId: t_id });
    		 that.getTileLayerConf();
    	 }, error:function (dd) {that.getTileLayerConf();}});
    	
    },
    // Try to load layer configuration from map_layers collection
    getTileLayerConf:function() {
    	var that=this;
    	this.layerCollection.fetch({
    		//Found configuration
    		success : function(collection, data) {
    			var d=data['result'];
    			// Give a default in case of no configuration
    			if (d.length==0) 
    				d=[{
                    	  name:"Mapquest OSM",
                    	  type:"MapQuest",
                    	  projection:"EPSG:3857",
						  layer:"osm"
                      }];
    				
    			
    			for (var index = 0; index < d.length; ++index) {
    			    if (d[index]['resolutions']) d[index]['resolutions']=JSON.parse(d[index]['resolutions']);
    			    if (d[index]['extent']) d[index]['extent']=JSON.parse(d[index]['extent']);
    			}
    			self.opts={layers:d};
    			
    			that.fetchMap();
    			
    		},
    		// Layers are not configured and layers collectio is not enabled
    		error: function(collection, data) {
    			self.opts={layers:[{
                   	  name:"Mapquest OSM",
                   	  type:"MapQuest",
                   	  projection:"EPSG:3857",
					  layer:"osm"
                }]};	
    			
    			that.fetchMap();	
    		}
    	});
    	
    },
    getTileLayers: function() {
    	if(typeof self.opts == 'undefined') {
    		this.getTileLayerConf();
    	}
    	else this.runTileLayers();
    },
    runTileLayers: function() {
		var layers=self.opts['layers'];
    	var that=this;
    	self.layers=new Array();
    	
    	//Layerswitcher output
    	var swhtml="<ul>";
    	
		/**
		Create layers
		**/
    	var l=0;
    	// Source
    	for (l = 0; l < layers.length; ++l) {
    		var layer=layers[l];
    		var sourceparms={};
    		if (layer['layer']) sourceparms['layer']=layer['layer'];
    		if (layer['url']) sourceparms['url']=layer['url'];
    		if (layer['maxResolution']) sourceparms['maxResolution']=layer['maxResolution'];
    		if (layer['type']=='TileWMS') {
    			sourceparms['params']={};
    			if (layer['layer']) sourceparms['params']['LAYERS']=layer['layer'];
        		if (layer['tiled']==1) sourceparms['params']['TILED']=true;
    		}
    		
    		if (layer['resolutions'] && layer['extent']) {
    			sourceparms['tileGrid']=new ol.tilegrid.XYZ({
		            extent: layer['extent'],
		            resolutions: layer['resolutions']
		        });
    		}
    		
    		if (!layer['projection']) layer['projection']='EPSG:3857';
    		if (l==self.activeLayer) {
    			
    			self.tileProjection=layer['projection'];
    			self.vectorProjection=layer['projection'];
    		}
    		sourceparms['visible']=false;
    		// Layer
    		if (layer['type']=='MapQuest') {
    			self.layers[l] = new ol.layer.Tile({
    				name:layer['name'],
    	    		 source:new ol.source.MapQuest(sourceparms)
    	    	});
    		}
    		else if (layer['type']=='TileWMS') {
    			sourceparms.params.VERSION='1.1.1';
    			self.layers[l] =new ol.layer.Tile({
    	    		name:layer['name'],
    	    	    extent: layer['extent'],
    	    	    
    	    	    source: new ol.source.TileWMS(/* @type {olx.source.TileWMSOptions} */ sourceparms)
    	    	  })
    		}
    		else if (layer['type']=='Yandex') {
    			sourceparms.tileUrlFunction=this.yandex_getTileURL;
    			self.layers[l] =new ol.layer.Tile({
    	    		name:layer['name'],
    	    	    extent: layer['extent'],
    	    	    source: new ol.source.XYZ(/* @type {olx.source.TileWMSOptions} */ sourceparms)
    	    	  })
    		}
    		
    		// Layerswitcher output
    		if (l==self.activeLayer) {
    			swhtml=swhtml+"<li><label><input type=\"radio\" name=\"layer\" value=\""+l+"\" checked>"+layer['name']+"</label></li>";
    		}
    		else swhtml=swhtml+"<li><label><input type=\"radio\" name=\"layer\" value=\""+l+"\">"+layer['name']+"</label></li>";
    		
    	}
		
    	swhtml=swhtml+"</ul>";
    	
    	$('#layerswitcher').html(swhtml);
    	$("#layerswitcher input[name=layer]").change(function() {that.switchLayer()});
    	
    },
    destroyMap: function() {
    	if (self.map) {
        	// ol3 has no proper method for destroy 
        	  self.map.setTarget(null);
        	  self.map=null;
    	}
    },
    
    fetchMap: function() {
    	
    	this.getTileLayers();
        
        /** Nice example: http://openlayers.org/en/v3.0.0/examples/vector-osm.html **/
        self.map = new ol.Map({
            target: 'map-content',
            controls: ol.control.defaults().extend([ new ol.control.ScaleLine({ units:'metric' }) ]),
            renderer: 'canvas',
            layers: self.layers,
            view: new ol.View({
            	projection: self.tileProjection,
          	    center: ol.proj.transform([27.4765837,57.7687628], 'EPSG:4326', self.tileProjection),
                zoom: 8
            })
        });
        self.layers[self.activeLayer].setVisible(true);
        /** Vector layers **/
        self.features = new Array();
        
        self.source = new ol.source.Vector({
        	  features: self.features
          });
        
        self.pointlayer = new ol.layer.Vector({
        	  source: self.source,
        	  style: this.setVectorStyle(),
        	  projection: self.vectorProjection,
        	  visible:1
          });
        
        self.map.addLayer(self.pointlayer);
        
        this.fetchLayer();
        
        var element = document.getElementById('map-popup');
        this.switchLayer();
        var popup = new ol.Overlay({
          element: element,
          positioning: 'bottom-center',
          stopEvent: false
        });
        self.map.addOverlay(popup);

     // display popup on click
        self.map.on('click', function(evt) {
          var feature = self.map.forEachFeatureAtPixel(evt.pixel,
              function(feature, layer) {
                return feature;
              });
          
          if (feature) {
            var geometry = feature.getGeometry();
            var coord = geometry.getCoordinates();
            popup.setPosition(coord);
            $(element).popover({
              'placement': 'top',
              'html': true,
              'content': '<h3><a href="'+feature.get('url')+'">'+feature.get('site_id')+' '+feature.get('name')+'</a></h3>'
              +feature.get('toponym')+'<hr />'
              	+feature.get('al1_mod')+','+feature.get('al2_mod')+','+feature.get('al3_mod')+','+feature.get('al4_mod')+'<hr />'
              	+feature.get('monument_type')+'<br />'+feature.get('object_type')+'<br />'+feature.get('period')+'<hr />'
            });
            $(element).popover('show');
          } else {
            $(element).popover('destroy');
          }
        });
       
        
    },
    render: function () {
      var opts={};
      var html = this.template({templates: this.templateCollection, templateId: this.templateId, permissions: Backbone.View.permissions()});
      this.$el.append(html);
	  self.activeLayer=0;
	  
      $('#map-content').show();
      $('#map-content').css('height',$(window).height() - 250);
      $(window).resize(function() {
    	  $('#map-content').css('height',$(window).height() - 250);
      });
      
      $('#fullscreen').click(function () {
    	  if ($('#map-content').hasClass("fullscreen")) {
    		  $('#map-content').removeClass("fullscreen");
    		  
    	  }
    	  else {
    		  $('#map-content').addClass("fullscreen");
    	  }
      });
      
      if (this.show_all) {
          this.resourceCollection.templateId = this.templateCollection.get(this.templateId).get('parent');
        }
      // Try to read layers configuration, upon success load map, else display only MapQuest OSM
      this.getConfig();
      //this.fetchMap();
      
      // Show map legends
      $( "#map-legend1" ).load( "/static/themes/default/legend1.html" );
      $( "#map-legend2" ).load( "/static/themes/default/legend2.html" );
      $( ".ui-draggable" ).draggable({ containment: "body",snap:true });
        
      $('.map-legend').show();
      
      return this;
    },

    onClose: function () {
      if (this.filter) {
        this.filter.remove();
      }
     
      if (self.map) {
    	// ol3 has no proper method for destroy 
    	  self.map.setTarget(null);
    	  self.map=null;
        $('#map-content').hide();
        $('.map-legend').html('');
        $('.map-legend').hide();
      }
    }
  });

  return ResourceGeoView;
});
