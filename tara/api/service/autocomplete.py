from tara.api.service.resource import ResourceService
from tara.core import mongo


class AutocompleteService():
    def __init__(self):
        self.service = ResourceService()

    def find(self, template_id, field):
        collection, template = self.service.collection(template_id)

        query = {field: {"$ne": None}, '_deleted': {'$exists': False}}

        if template['parent']:
            query['_template_id'] = template_id

        return {'result': collection.find(query).distinct(field)}

    def find_by_collection(self, collection_name, field):
        try:
            query = {field: {"$ne": None}, '_deleted': {'$exists': False}}
            return {'result': mongo.db[collection_name].find(query).distinct(field)}
        except:
            return {'result':[]}