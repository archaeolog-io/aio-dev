define(['backbone', 'backbone-forms'], function (Backbone) {
  "use strict";

  var TextAreaJSONSmallEditor = Backbone.Form.editors.TextArea.extend({
    getValue: function () {
      var value = this.$el.val();
      if (value) {
        return JSON.parse(value);
      }
    },

    setValue: function (value) {
      if (value) {
        var str = JSON.stringify(value, null, '  ');
        this.$el.val(str);
      }
    }
  });

  return TextAreaJSONSmallEditor;
});
