from flask import request
from flask.views import MethodView

from tara import factory, core, helpers

from tara.api.util.converters import ObjectIDConverter
from tara.api.util.error_handlers import init_error_handlers
from tara.api.util.json_encoder import MongoJsonEncoder
from tara.api.util.authentication import configure_auth
from tara.api.util.api_docs import configure_docs
from tara.api.util.logging import configure_logging
from tara.api.util.decorators import login_required, return_json


class BaseMethodView(MethodView):
    decorators = [return_json]

    @login_required
    def get(self, _id):
        return self.service.find(_id)

    @login_required
    def post(self):
        data = request.get_json()
        _id = self.service.insert(data)
        return self.service.find(_id)

    @login_required
    def patch(self, _id):
        data = request.get_json()
        self.service.update(_id, data)
        return self.service.find(_id)

    @login_required
    def delete(self, _id):
        result = self.service.find(_id)
        self.service.remove(_id)
        return result


def register_api(bp, view, url_prefix='', pk='_id', pk_type='ObjectId'):
    view_func = view.as_view(bp.name)

    # if this attribute is set Flask will either force enable or disable the automatic implementation of the
    # HTTP OPTIONS response. This can be useful when working with decorators that want to customize the OPTIONS
    # response on a per-view basis.
    view_func.provide_automatic_options = False

    if 'POST' and 'PATCH' and 'DELETE' in view.methods:
        bp.add_url_rule(url_prefix + '/', defaults={'_id': None}, view_func=view_func, methods=['GET'])
        bp.add_url_rule(url_prefix + '/', view_func=view_func, methods=['POST'])
        bp.add_url_rule(url_prefix + '/<%s:%s>' % (pk_type, pk), view_func=view_func,
                        methods=['GET', 'PATCH', 'DELETE'])
    else:
        bp.add_url_rule('/<%s:%s>' % (pk_type, pk), view_func=view_func, methods=['GET'])


def create_app(**settings_override):
    """Returns the API application instance"""
    app = factory.create_app(__name__, settings_override, static_folder=None)

    # setup mongo database
    core.mongo.init_app(app)
    app.mongo = core.mongo

    # setup bcrypt
    core.bcrypt.init_app(app)

    # setup login manager
    core.login_manager.init_app(app)

    # configure authentication
    configure_auth(app)

    # url converter
    app.url_map.converters['ObjectId'] = ObjectIDConverter

    # error handlers
    init_error_handlers(app)

    # set json encoder
    app.json_encoder = MongoJsonEncoder

    # blueprints
    helpers.register_blueprints(app, __name__, __path__)

    # api docs
    configure_docs(app)

    # configure loggings
    configure_logging(app)

    return app
