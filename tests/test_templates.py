import unittest
import json

from tara.api import controller as api

UNITTEST_TEMPLATE = dict(
    label='_unittestname_',
    parent=None,
    grid_fields=[
        {
            "name": "textfield",
            "label": "Textfield",
            "type": "string"
        },
        {
            "name": "textareafield",
            "label": "Textareafield",
            "type": "string"
        }
    ],
    form_fields=[
        {
            "name": "textfield",
            "label": "Textfield",
            "type": "text"
        },
        {
            "name": "textareafield",
            "label": "Textareafield",
            "type": "textarea"
        }
    ]
)


class TemplateTestCase(unittest.TestCase):
    def setUp(self):
        app = api.create_app(TESTING=True)
        self.client = app.test_client()

        rv = self.client.get('/templates/')
        self.assertEqual(rv.status_code, 200)

        templates = json.loads(rv.data)['result']
        to_remove = filter(lambda k: k is UNITTEST_TEMPLATE['label'], templates)

        for item in to_remove:
            app.logger.warn('Cleaning up previous tests')
            self.test_delete_template(item['_id'])

    def test_delete_template(self, _id=None):
        if _id:
            rv = self.client.delete('/templates/' + _id)
            self.assertEqual(rv.status_code, 200)

    def test_create_template(self, cleanup=True):
        # create template
        json_string = json.dumps(UNITTEST_TEMPLATE)
        rv = self.client.post('/templates/', data=json_string, content_type='application/json')
        self.assertEqual(rv.status_code, 200, rv.data)
        self.assertEqual(rv.content_type, 'application/json')

        # get template id
        _id = json.loads(rv.data)['_id']
        self.assertEqual(len(_id), 24)

        if cleanup:
            self.test_delete_template(_id)

        return _id

    def test_update_template(self):
        # create template
        _id = self.test_create_template(cleanup=False)
        json_string = json.dumps(UNITTEST_TEMPLATE)

        # update template
        rv = self.client.patch('/templates/'+_id, data=json_string, content_type='application/json')
        self.assertEqual(rv.status_code, 200, str(rv.data))
        self.assertEqual(rv.content_type, 'application/json')

        # delete template
        rv = self.client.delete('/templates/' + _id)
        self.assertEqual(rv.status_code, 200)

        # delete non-existing template
        rv = self.client.delete('/templates/' + _id)
        self.assertEqual(rv.status_code, 404)

        # try retrive deleted template
        rv = self.client.get('/templates/'+_id)
        self.assertEqual(rv.status_code, 404)