define(['backbone', 'helpers/textarea-json', 'translator'], function (Backbone, TextAreaJSONEditor, T) {
  "use strict";

  var TemplateLayoutForm = Backbone.Form.extend({
    initialize: function (options) {

      options.schema  = {
        label: {
          title: T('layout-label'),
          type: 'Text',
          validators: ['required']
        },
        sort: {
          title: T('layout-sort'),
          type: 'Number'
        },
        widgets: {
          title: T('layout-widgets'),
          type: TextAreaJSONEditor,
          validators: ['required']
        }
      };

      Backbone.Form.prototype.initialize.call(this, options);
    }
  });

  return TemplateLayoutForm;
});
