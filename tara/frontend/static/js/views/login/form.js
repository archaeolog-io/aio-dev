define(['translator', 'backbone-forms'], function (T) {
  "use strict";

  var LoginForm = Backbone.Form.extend({
    schema: {
      username: {
        title: T('login-username'),
        type: 'Text',
        validators: ['required']
      },
      password: {
        title: T('login-password'),
        type: 'Password',
        validators: ['required']
      }
    }
  });

  return LoginForm;
});
