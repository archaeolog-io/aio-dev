define(['backbone', 'backbone-forms'], function (Backbone) {
  "use strict";
/**
 * Added by Miina 08/10/2014
 * Object for managing text field dealing with array displayd as a string with separators
 */
  var InputArraySeparatorEditor = Backbone.Form.editors.Text.extend({
    initialize: function (options) {
    	//If the separator is not set
        if( !options.schema.separator ) {
      	  this.separator = ',';
        }
        else {
      	  //Take the separator from options
        	this.separator = options.schema.separator;
        }
        
      Backbone.Form.editors.Base.prototype.initialize.call(this, options);
    },

    getValue: function () {
    	//Remove spaces just in case
    	var rawValue = this.$el.val().replace(/ /g, "");
    	//Split the raw Value to an array
    	return rawValue.split( this.separator );
    },
    setValue: function (value) {
        if (value) {
        	//Change the array to a string
        	value = value + "";
        	//Replace the commas with the separator
          var str = value.replace(/,/g, this.separator);
          //Set the value
          this.$el.val(str);
        }
      }
  });
  return InputArraySeparatorEditor;
});
