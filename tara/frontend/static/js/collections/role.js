define(['backbone', 'models/role'], function (Backbone, RoleModel) {
  "use strict";

  var RoleCollection = Backbone.Collection.extend({
    url: '/api/roles/',

    model: RoleModel,

    parse: function (response) {
      return response.result;
    }
  });

  return RoleCollection;
});
