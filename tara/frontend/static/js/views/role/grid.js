define(['backgrid', 'translator'], function (Backgrid, T) {
  "use strict";

  var RoleGrid = Backgrid.Grid.extend({
    initialize: function (options) {
      options.columns = [
        {
          name: 'name',
          label: T('role-name'),
          editable: false,
          cell: 'string'
        },
        {
          name: '_action',
          label: '',
          editable: false,
          cell: Backgrid.Cell.extend({
            render: function () {
              var tmpl = '<a href="/roles/<%= _id %>" class="btn">' + T('button-edit') + '</a>';
              this.$el.html(_.template(tmpl, this.model.attributes));
              return this;
            }
          })
        }
      ];

      this.constructor.__super__.initialize.apply(this, [options]);
    }
  });

  return RoleGrid;
});
