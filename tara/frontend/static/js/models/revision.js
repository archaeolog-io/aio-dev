define(['backbone'], function (Backbone) {
  "use strict";

  var RevisionModel = Backbone.Model.extend({
    idAttribute: '_id'
  });

  return RevisionModel;
});
