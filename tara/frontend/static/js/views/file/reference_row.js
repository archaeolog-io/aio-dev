define(['models/autocomplete', 'collections/resource', 'text!/static/templates/file/reference_row.html', 'translator', 'backbone-forms'], function (AutocompleteModel, ResourceCollection, fileReferenceRowTmpl, T) {
  'use strict';

  var FileReferenceRow = Backbone.View.extend({
    templates: null,

    _template_id: null,

    _reference_id: null,

    _description: null,

    tagName: 'li',

    className: 'reference-row',

    events: {
      'change select[name=template_id]': function (e) {
        e.preventDefault();
        this.$el.find('#reference-finder').empty();
        this.$el.find('#reference-finder-button').hide();

        var template_id = e.target.value;
        if (!template_id) {
          return;
        }

        var unique = this.templates.get(template_id).attributes.unique;
        if (!unique) {
          return;
        }

        for (var i = 0; i < unique.length; i++) {
          var input = $('<input>').attr({
              type: 'text',
              name: unique[i],
              placeholder: unique[i]
          }).typeahead({
            source: (function (uid) {
              return function (query, process) {
                var autocomplete = new AutocompleteModel({}, {templateId: template_id, field: uid});
                autocomplete.fetch({success: function () {
                  process(autocomplete.toJSON().result);
                }});
              }
            })(unique[i]),
            updater: function (item) {
              return '^' + item + '$';
            }
          });

          this.$el.find('#reference-finder').append(input);
          this.$el.find('#reference-finder-button').show();
        }
      },
      'click button[name=find_reference]': function (e) {
        e.preventDefault();

        var template_id = this.$el.find('select[name=template_id]').val();
        if (!template_id) {
          return;
        }

        var unique = this.templates.get(template_id).attributes.unique;
        if (!unique) {
          return;
        }

        var resources = new ResourceCollection([], {templateId: template_id});
        for (var i = 0; i < unique.length; i++) {
          resources.queryParams[unique[i]] = this.$el.find('input[name='+unique[i]+']').val();
        }

        var self = this;

        resources.fetch({success: function () {
          if (resources.length == 1) {
            self.$el.find('input[name=resource_id]').val(resources.models[0].id);
          } else {
            self.$el.find('input[name=resource_id]').val('');
            alert('Seost ei leitud');
          }
        }});
      }
    },

    initialize: function (options) {
      this.templates = options.templates;
    },

    render: function () {
      this.$el.html(_.template(fileReferenceRowTmpl, {
        templates: this.templates,
        _id: this._id,
        template_id: this._template_id,
        resource_id: this._resource_id,
        description: this._description
      }));
      return this;
    },

    getValue: function () {
      return {
        template_id: this.$el.find('select[name=template_id]').val(),
        resource_id: this.$el.find('input[name=resource_id]').val(),
        description: this.$el.find('textarea[name=description]').val()
      };
    }
  });

  return FileReferenceRow;
});
