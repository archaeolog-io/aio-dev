define(['backbone', 'models/template'], function (Backbone, TemplateModel) {
  "use strict";

  var TemplateCollection = Backbone.Collection.extend({
    url: '/api/templates/',

    model: TemplateModel,

    parse: function (response) {
      return response.result;
    }
  });

  return TemplateCollection;
});
