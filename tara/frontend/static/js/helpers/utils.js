define([''], function () {
  "use strict";

  var Utils = {
    getWeekByDate: function (date) {
    	var year = date.getFullYear();
    	var firstJan = new Date(year,0,1);
    	var firstJanDay = firstJan.getDay()-1;
    	var dayOfYear = Math.ceil((date - firstJan + 86400000)/86400000);
    	
    	if(firstJanDay<4){
    		var week = Math.floor((dayOfYear+firstJanDay-1)/7) + 1;
    		if(week > 52) {
    			nYear = new Date(this.getFullYear() + 1,0,1);
    			nday = nYear.getDay()-1;
    			nday = nday >= 0 ? nday : nday + 7;
    			/*if the next year starts before the middle of
     			  the week, it is week #1 of that year*/
    			week = nday < 4 ? 1 : 53;
    		}
    	}
    	else {
    		var week=Math.ceil((dayOfYear+firstJanDay-1)/7);
    	}
    	return week;
    },
     
    getSundayByWeekAndYear: function(y,w){
    	var days = 2 + 6 + (w - 1) * 7 - (new Date(y,0,1)).getDay();
    	var newDate = new Date(y, 0, days);
        return newDate.getFullYear() + '-' + (newDate.getMonth()+1) + '-' + newDate.getDate();
    }
  };

  return Utils;
});