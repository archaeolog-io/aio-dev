from flask import Blueprint, request

from tara.api.controller import BaseMethodView, register_api
from tara.api.service.resource import ResourceService

from tara.api.util.decorators import login_required


class ResourceView(BaseMethodView):
    def __init__(self):
        self.service = ResourceService()

    @login_required
    def get(self, template_id, _id,**kwargs):
        rargs=request.args.copy()
    # do we have role based limitations?
        if 'force_value' in kwargs:
            for key, value in kwargs['force_value'].iteritems():
                rargs[key]=value
        return self.service.find(template_id, _id, rargs)

    @login_required
    def post(self, template_id,**kwargs):
        data = request.get_json()
        for key, value in request.files.iteritems(multi=True):
            _id = value
    # do we have role based limitations?
        if 'force_value' in kwargs:
            for key, value in kwargs['force_value'].iteritems():
                data[key]=value
        _id = self.service.insert(template_id, data)
        return self.get(template_id, _id)

    @login_required
    def patch(self, template_id, _id,**kwargs):
        data = request.get_json()
    # do we have role based limitations?
        if 'force_value' in kwargs:
            for key, value in kwargs['force_value'].iteritems():
                data[key]=value
    # check if our item is actually writeable for role?
                d=self.service.find(template_id, _id, kwargs['force_value'])
                if d:
                    self.service.update(template_id, _id, data)
        else:
            self.service.update(template_id, _id, data)
        res = self.get(template_id, _id)
        res['_patched']=True
        return res

    @login_required
    def delete(self, template_id, _id,**kwargs):
        result = self.get(template_id, _id,**kwargs)
    # do we have role based limitations?
        if 'force_value' in kwargs:
            # check if our item is actually writeable for role?
            d=self.service.find(template_id, _id, kwargs['force_value'])
            if d:
                self.service.remove(template_id, _id)
        else:
            self.service.remove(template_id, _id)
        
        return result


bp = Blueprint('resource', __name__, url_prefix='/resources')
register_api(bp, ResourceView, url_prefix='/<ObjectId:template_id>')