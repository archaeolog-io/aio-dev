from flask import Blueprint, request

from tara.api.controller import BaseMethodView, register_api
from tara.api.service.classificator import ClassificatorService
from tara.api.util.decorators import login_required


class ClassificatorView(BaseMethodView):
    def __init__(self):
        self.service = ClassificatorService()

    @login_required
    def get(self, _id):
        return self.service.find(_id, request.args)


bp = Blueprint('classificator', __name__, url_prefix='/classificators')
register_api(bp, ClassificatorView)
