define(['collections/classificator', 'views/classificator/grid', 'text!/static/templates/classificator/list.html'], function (ClassificatorCollection, ClassificatorGrid, classificatorListTmpl) {
  "use strict";

  var ClassificatorListView = Backbone.View.extend({
    template: _.template(classificatorListTmpl),

    initialize: function () {
      this.collection = new ClassificatorCollection();
      this.grid = new ClassificatorGrid({collection: this.collection});
    },

    render: function () {
      this.$el.append(this.template());
      this.$el.find('#grid-content').append(this.grid.render().$el);
      this.collection.fetch({reset: true});
      return this;
    },

    onClose: function () {
      this.grid.remove();
    }
  });

  return ClassificatorListView;
});
