define(['backbone', 'models/file', 'backbone-pageable'], function (Backbone, FileModel) {
  "use strict";

  var FileCollection = Backbone.PageableCollection.extend({
    url: '/api/files/',

    model: FileModel,

    queryParams: {
      directions: {
        '-1': 1,
        '1': -1
      }
    },

    parseRecords: function (response) {
      return response.result;
    },

    parseState: function (response) {
      return {totalRecords: response.total_entries};
    }
  });

  return FileCollection;
});
