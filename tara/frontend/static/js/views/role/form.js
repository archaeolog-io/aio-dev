define(['helpers/textarea-json', 'translator', 'backbone-forms'], function (TextAreaJSONEditor, T) {
  "use strict";

  var RoleForm = Backbone.Form.extend({
    schema: {
      name: {
        title: T('role-name'),
        type: 'Text',
        validators: ['required']
      },
      description: {
        title: T('role-description'),
        type: 'TextArea'
      },
      permissions: {
        title: T('role-editor'),
        type: TextAreaJSONEditor,
        validators: ['required']
      }
    }
  });

  return RoleForm;
});
