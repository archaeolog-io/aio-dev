from flask import Blueprint

from tara.api.controller import BaseMethodView, register_api
from tara.api.service.template import TemplateService


class TemplateView(BaseMethodView):
    def __init__(self):
        self.service = TemplateService()

    def get(self, _id):
        return self.service.find(_id)

bp = Blueprint('template', __name__, url_prefix='/templates')
register_api(bp, TemplateView)
